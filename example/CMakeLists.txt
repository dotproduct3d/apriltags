#set(OpenCV_DIR "/opt/local/lib/cmake" CACHE STRING "")
find_package(OpenCV REQUIRED)

set(SOURCE_FILES
  apriltags_demo.cpp
)

add_executable(apriltags_demo ${SOURCE_FILES})
target_include_directories(apriltags_demo PUBLIC ${OpenCV_INCLUDE_DIRS})
target_link_libraries(apriltags_demo AprilTags ${OpenCV_LIBS})
