#!/usr/bin/env python
__author__ = 'arh'

# This script takes a directory full of AprilTag pngs and turns them
# into a PDF with Tag IDs printed under each of the tags.
# It depends on:
# potrace
# ImageMagick ('convert' command)
# pdflatex

import errno
import getopt
import os
import sys

from subprocess import Popen
from subprocess import check_call


def mkdir_p(path):
    try:
        os.makedirs(path)
    except OSError as exc:  # Python >2.5
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else:
            raise


def help_message():
    print os.path.basename(__file__) + \
        " -d <input_directory> -p <input_pattern> [-f <family_name>] " + \
        "[-m <max_images>] -o <output_pdf>"
    print "\nExample:"
    print os.path.basename(__file__) + \
        " -d ~/tag36h11 -p tag36_11_%05d.png -o tags.pdf"


def process_args(argv):
    input_dir = ''
    input_pattern = ''
    output_pdf = ''
    family_name = ''
    max_images = None

    try:
        opts, args = getopt.getopt(argv, "hd:p:o:m:f:")
    except getopt.GetoptError:
        help_message()
        sys.exit()

    for opt, arg in opts:
        if opt == '-h':
            help_message()
            sys.exit()
        elif opt == "-d":
            input_dir = arg
        elif opt == "-p":
            input_pattern = arg
        elif opt == "-o":
            output_pdf = arg
        elif opt == "-m":
            max_images = int(arg)
        elif opt == "-f":
            family_name = arg

    if not (input_dir and input_pattern and output_pdf):
        help_message()
        sys.exit()

    return input_dir, input_pattern, output_pdf, family_name, max_images


def main(argv):
    (input_dir, input_pattern, output_pdf, family_name, max_images) = \
        process_args(argv)

    temp_dir = "apriltag_temp"
    mkdir_p(temp_dir)

    # Convert tiny pngs into pdf files
    num_images = 0
    while True:
        if max_images and num_images >= max_images:
            break

        input_image = os.path.join(input_dir, input_pattern % num_images)
        if not os.path.isfile(input_image):
            break

        # First go to a larger bitmap format that potrace can import
        pbm_image = os.path.join(temp_dir, "%05d.pbm" % num_images)
        if not os.path.isfile(pbm_image):
            print "converting image " + input_image + " -> " + pbm_image
            check_call(["convert", input_image, "-filter", "Point", "-resize",
                        "x800", pbm_image])

        # Now run potrace to convert to a vector format
        # We use the alphamax=0 to try and get sharp corners
        pdf_image = os.path.join(temp_dir, "%05d.pdf" % num_images)
        if not os.path.isfile(pdf_image):
            print "tracing image " + pbm_image + " -> " + pdf_image
            check_call(["potrace", "--alphamax", "0", pbm_image, "-b", "pdf",
                        "-o", pdf_image])

        num_images += 1

    # Write the latex file
    tex_file_name = "april.tex"
    tex_file = os.path.join(temp_dir, tex_file_name)
    with open(tex_file, "w") as f:
        f.write("\\documentclass[12pt]{article}\n")
        f.write("\\usepackage{graphicx}\n")
        f.write("\\usepackage{float}\n")
        f.write("\\usepackage[margin=0cm]{geometry}\n")
        f.write("\\usepackage{helvet}\n")
        f.write("\\begin{document}\n")

        for i in range(0, num_images):
            pdf_image = ("%05d.pdf" % i)
            f.write("\\begin{figure}[H]\n")
            f.write("\\centering\n")
            f.write("\\includegraphics[width=\\textwidth]{" + pdf_image + "}\n")

            tag_id = "%05d" % i
            if family_name:
                text_string = family_name + " - " + tag_id
            else:
                text_string = tag_id

            f.write("\\fontfamily{phv}\\selectfont{\\Huge{\\bfseries{")
            f.write(text_string + "}}}\n")
            f.write("\\end{figure}\n")
            f.write("\\pagebreak\n")

        f.write("\\end{document}\n")

    # Now run pdflatex on the tex file
    print "Running pdflatex..."
    pdflatex_process = Popen(["pdflatex", tex_file_name], cwd=temp_dir)
    pdflatex_process.wait()
    print "Done"

    # Copy the pdf file to the requested output location
    temp_output_pdf = os.path.join(temp_dir, "april.pdf")
    check_call(['cp', temp_output_pdf, output_pdf])

if __name__ == "__main__":
    main(sys.argv[1:])