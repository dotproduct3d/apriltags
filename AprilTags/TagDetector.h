#ifndef TAGDETECTOR_H
#define TAGDETECTOR_H

#include <vector>

#include <boost/exception/all.hpp>

#include "AprilTags/ImageTypes.h"
#include "AprilTags/TagDetection.h"
#include "AprilTags/TagFamily.h"
#include "AprilTags/FloatImage.h"

#include "AprilTags/Visibility.h"

namespace AprilTags {

namespace exceptions {
  struct TagDetectorException: virtual std::exception, virtual boost::exception {};
}

class DLL_PUBLIC TagDetector {
public:

  const TagFamily thisTagFamily;

  //! Constructor
  // note: TagFamily is instantiated here from TagCodes
  TagDetector(const TagCodes& tagCodes) : thisTagFamily(tagCodes) {}

  std::vector<TagDetection> extractTags(const ImageType& image) const;

private:
};

} // namespace

#endif
