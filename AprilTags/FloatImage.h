#ifndef FLOATIMAGE_H
#define FLOATIMAGE_H

#include <algorithm>
#include <vector>
#include "cvd/image.h"

#include "AprilTags/ImageTypes.h"

namespace AprilTags {

//! Represent an image as a vector of floats in [0,1]
typedef CVD::Image<float> FloatImage;

/**
 * @brief Convert uint8 image into float image with values in range [0,1]
 * @param grayImage Input image in uint8 format
 * @param outImage Float image to be filled with result (may be resized if necessary)
 */
void convertByteImageToFloatImage(const ImageType& grayImage,
                                  FloatImage &outImage);

} // namespace

#endif
