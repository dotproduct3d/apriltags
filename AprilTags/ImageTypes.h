#ifndef IMAGETYPES_H
#define IMAGETYPES_H

#include <stdint.h>
#include "cvd/image.h"

typedef CVD::BasicImage<uint8_t> ImageType;

#endif // IMAGETYPES_H
