#ifndef TAGDETECTION_H
#define TAGDETECTION_H

#include <Eigen/Dense>


#include <utility>
#include <vector>

namespace AprilTags {

struct TagDetection {

  //! Constructor
  TagDetection();

  //! Constructor for manually creating tags in a world map
  TagDetection(int id);

  //! Is the detection good enough?
  bool good;

  //! Observed code
  long long obsCode;

  //! Matched code
  long long code;

  //! What was the ID of the detected tag?
  int id;

  //! The hamming distance between the detected code and the true code
  int hammingDistance;
  
  //! How many 90 degree rotations were required to align the code (internal use only)
  int rotation;

  /////////////// Fields below are filled in by TagDetector ///////////////
  //! Position (in fractional pixel coordinates) of the detection.
  /*!  The points travel COUNTER-CLOCKWISE around the target, always
   *  starting from the BOTTOM LEFT corner of the tag.
   *
   *  4 ---- 3
   *  |      |  <--- ordering
   *  |      |
   *  0------1
   */
  std::pair<float,float> p[4];

  //! Center of tag in pixel coordinates.
  std::pair<float,float> cxy;

  //! Measured in pixels, how long was the observed perimeter.
  /*! Observed perimeter excludes the inferred perimeter which is used to connect incomplete quads. */
  float observedPerimeter;

  //! Used to eliminate redundant tags
  bool overlapsTooMuch(const TagDetection &other) const;
};

} // namespace

#endif
