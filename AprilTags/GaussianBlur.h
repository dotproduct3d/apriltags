#ifndef GAUSSIANBLUR_H
#define GAUSSIANBLUR_H

#include "cvd/convolution.h"

#include "AprilTags/FloatImage.h"

namespace AprilTags {

// This is adapted from libcvd's convolveSeparableSymmetric function
template <class T, class K> void convolveSeparableSymmetricCentred(
    CVD::Image<T>& I, const std::vector<K>& kernel, K divisor)
{
  typedef typename CVD::Pixel::traits<T>::wider_type sum_type;
  int width = I.size().x;
  int height = I.size().y;
  int r = (int)kernel.size()/2; // Kernel 'radius' (ie. pixels either side of center)
  double factor = 1.0/divisor;

  // We can't do the operation in-place, because we need access to the original pixels
  // This is a temporary vector for the original column, padded with zeros to deal with
  // boundaries
  std::vector<T> tempCol(height + 2*r, 0.0);

  // Loop through columns and blur each independently
  for (int j=0; j<width;++j) {

    // Pointer to first pixel in the column
    T* p = I.data() + j;

    // Copy original values into the temporary storage
    for (int i=0; i<height; ++i, p+=width) {
      tempCol[i+r] = *p;
    }

    // Reset pixel pointer
    p = I.data() + j;

    // Loop down the column computing kernel result
    for (int i=0; i<height; ++i, p+=width) {
      // Deal with middle pixel first
      sum_type sum = tempCol[i+r]*kernel[r];

      // Now loop over remainder of kernel, taking advantage of its symmetricity
      for (int m=0; m<r; ++m) {
        sum += (tempCol[i+m] + tempCol[i+2*r-m]) * kernel[m];
      }

      // Update pixel value in input image
      *p = static_cast<T>(sum * factor);
    }
  }

  // Temp storage for the row
  std::vector<T> tempRow(width + 2*r, 0.0);

  // Loop through rows and blur each independently
  for (int i=0; i<height; ++i) {
    // Pointer to first pixel in row
    T* p = I[i];

    // Copy original values into the temporary storage
    for (int j=0; j<width; ++j) {
      tempRow[j+r] = p[j];
    }

    // Reset pixel pointer
    p = I[i];

    // Loop along the row computing kernel result
    for (int j=0; j<width; ++j) {
      // Deal with middle pixel first
      sum_type sum = tempRow[j+r]*kernel[r];

      // Now loop over remainder of kernel, taking advantage of its symmetricity
      for (int m=0; m<r; ++m) {
        sum += (tempRow[j+m] + tempRow[j+2*r-m]) * kernel[m];
      }

      // Update pixel value in input image
      p[j] = static_cast<T>(sum*factor);
    }
  }
}

void blurImage(FloatImage& floatImage, int filterSize, float sigma);

} // namespace AprilTags

#endif // GAUSSIANBLUR_H
