# AprilTags library
LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
LOCAL_MODULE := Apriltags

LOCAL_SRC_FILES := src/Edge.cc \
                   src/FloatImage.cc \
                   src/GLine2D.cc \
                   src/GLineSegment2D.cc \
                   src/GaussianBlur.cc \
                   src/GrayModel.cc \
                   src/MathUtil.cc \
                   src/Quad.cc \
                   src/Segment.cc \
                   src/TagDetection.cc \
                   src/TagDetector.cc \
                   src/TagFamily.cc \
                   src/UnionFindSimple.cc

LOCAL_C_INCLUDES += $(LOCAL_PATH)
LOCAL_C_INCLUDES += $(ANDROID_INCLUDE_PATH)

LOCAL_EXPORT_C_INCLUDES += $(LOCAL_PATH)
LOCAL_SHARED_LIBRARIES += androidCVD
LOCAL_LDLIBS += -llog
include $(BUILD_SHARED_LIBRARY)

include $(CLEAR_VARS)
