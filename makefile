# Enable a DEBUG build by passing "DEBUG=1" to the make command
LOCALDIR=$(shell pwd)

# -----------------------------------

CC=clang++
DEPEND_DIR=.depend
BUILD_DIR=build

APRIL_LIB_NAME = libApriltags.dylib

# -----------------------------------

INCLUDE_PATHS += -I$(LOCALDIR) \
                 -I/opt/local/include \
                 -I/usr/local/include \
                 -I/usr/include

LIB_PATHS = -L/opt/local/lib \
            -L/usr/lib \
            -L/usr/local/lib \
            -F/System/Library/Frameworks

# Flags common to both Release and Debug builds
CFLAGS_COMMON = -c -Wall -Wextra -D_OSX -D__APPLE__

ifdef LICENSING
else
  # Not enabled for licensing, because it doesn't seem to work properly with lib crypto
  CFLAGS_COMMON+=-fvisibility=hidden
endif

# These make sure you still get correctly formatted output from the compiler if using ccache
CFLAGS_COMMON += -Qunused-arguments -fcolor-diagnostics -fPIC

CFLAGS_RELEASE = $(CFLAGS_COMMON) -O3 -DNDEBUG
CFLAGS_DEBUG = $(CFLAGS_COMMON) -O0 -g3

LDFLAGS_COMMON  = -dynamiclib -Wl,-rpath,. -fPIC
LDFLAGS_RELEASE = $(LDFLAGS_COMMON)
LDFLAGS_DEBUG = $(LDFLAGS_COMMON) -g3

ifdef DEBUG
  $(info "Running DEBUG build")
  CFLAGS=$(CFLAGS_DEBUG)
  LDFLAGS=$(LDFLAGS_DEBUG)
else
  ifdef MEMDEBUG
    $(info "Running MEMDEBUG build")
    CFLAGS=$(CFLAGS_DEBUG) -DMEMDEBUG
    LDFLAGS=$(LDFLAGS_DEBUG)
  else
    $(info "Running RELEASE build")
    CFLAGS=$(CFLAGS_RELEASE)
    LDFLAGS=$(LDFLAGS_RELEASE)
  endif
endif

ifdef PROFILE
  CFLAGS += -g3
endif

#######################
# Default make target
.PHONY: all
all: april

# Make build directory
.PHONY: $(BUILD_DIR)
$(BUILD_DIR):
	@mkdir -p $(BUILD_DIR)

##############################
# Build the AprilTags library
.PHONY: april
april : $(APRIL_LIB_NAME)

APRIL_SOURCES = Edge.cc \
                FloatImage.cc \
                GLine2D.cc \
                GLineSegment2D.cc \
                GaussianBlur.cc \
                GrayModel.cc \
                MathUtil.cc \
                Quad.cc \
                Segment.cc \
                TagDetection.cc \
                TagDetector.cc \
                TagFamily.cc \
                UnionFindSimple.cc

APRIL_LINKED_LIBS = -lcvd

APRIL_OBJECTS = $(patsubst %.cc, $(BUILD_DIR)/%.o, $(APRIL_SOURCES))

$(APRIL_OBJECTS) : | $(BUILD_DIR)

# The main library target
# This -install_name is necessary so that in the linking step for executables using the library, they are told
# where to look for it (if it's not in the same directory as the executable)
$(APRIL_LIB_NAME) : $(APRIL_OBJECTS)
	ar rcs $@ $^

# A Generic rule for building object files, which also generates dependencies
$(BUILD_DIR)/%.o : src/%.cc
	mkdir -p $(DEPEND_DIR)/$$(dirname "$*")
	$(CC) $(CFLAGS) $(INCLUDE_PATHS) -MMD -MF $(DEPEND_DIR)/$*.d $< -o $@

#################
# Other targets

DEPS_SOURCES = $(APRIL_SOURCES)
DEPS = $(patsubst %.cc, $(DEPEND_DIR)/%.d, $(DEPS_SOURCES))

# We don't want to include .depend if we've just done a make clean as it won't
# exist and then we'll end up regenerating it and all its dependencies
ifneq ($(MAKECMDGOALS),clean)
-include $(DEPS)
endif

.PHONY: clean
clean:
	rm -rf $(BUILD_DIR) $(DEPEND_DIR)
	rm -rf $(APRIL_LIB_NAME)

