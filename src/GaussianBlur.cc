#include "AprilTags/GaussianBlur.h"

namespace AprilTags {

void blurImage(FloatImage &floatImage, int filterSize, float sigma) {
  std::vector<float> filterKernel(filterSize, 0.0);

  float kernelSum = CVD::gaussianKernel<float>(filterKernel, 1.0, sigma);
  AprilTags::convolveSeparableSymmetricCentred(floatImage, filterKernel, kernelSum);
}

} // namespace AprilTags
