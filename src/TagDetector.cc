#include <algorithm>
#include <cmath>
#include <climits>
#include <map>
#include <vector>
#include <iostream>

#include <Eigen/Dense>
#include "cvd/convolution.h"

#include "AprilTags/ImageTypes.h"
#include "AprilTags/Edge.h"
#include "AprilTags/FloatImage.h"
#include "AprilTags/GaussianBlur.h"
#include "AprilTags/GrayModel.h"
#include "AprilTags/GLine2D.h"
#include "AprilTags/GLineSegment2D.h"
#include "AprilTags/Gridder.h"
#include "AprilTags/MathUtil.h"
#include "AprilTags/Quad.h"
#include "AprilTags/Segment.h"
#include "AprilTags/TagFamily.h"
#include "AprilTags/UnionFindSimple.h"
#include "AprilTags/XYWeight.h"
#include "AprilTags/TagDetector.h"

using namespace std;

namespace AprilTags {

std::vector<TagDetection> TagDetector::extractTags(const ImageType& image) const {

  int height = image.size().y;
  int width = image.size().x;

  FloatImage fimOrig;
  convertByteImageToFloatImage(image, fimOrig);

  //================================================================
  // Step one: preprocess image (convert to grayscale) and low pass if necessary

  FloatImage fim(fimOrig.copy_from_me());
  
  //! Gaussian smoothing kernel applied to image (0 == no filter).
  /*! Used when sampling bits. Filtering is a good idea in cases
   * where A) a cheap camera is introducing artifical sharpening, B)
   * the bayer pattern is creating artifcats, C) the sensor is very
   * noisy and/or has hot/cold pixels. However, filtering makes it
   * harder to decode very small tags. Reasonable values are 0, or
   * [0.8, 1.5].
   */
  float sigma = 0;

  //! Gaussian smoothing kernel applied to image (0 == no filter).
  /*! Used when detecting the outline of the box. It is almost always
   * useful to have some filtering, since the loss of small details
   * won't hurt. Recommended value = 0.8. The case where sigma ==
   * segsigma has been optimized to avoid a redundant filter
   * operation.
   */
  float segSigma = 0.8f;

  if (sigma > 0) {
    int filterSize = ((int) max(3.0f, 3*sigma)) | 1;
    blurImage(fim, filterSize, sigma);
  }

  //================================================================
  // Step two: Compute the local gradient. We store the direction and magnitude.
  // This step is quite sensitve to noise, since a few bad theta estimates will
  // break up segments, causing us to miss Quads. It is useful to do a Gaussian
  // low pass on this step even if we don't want it for encoding.
  FloatImage fimSeg;
  if (segSigma > 0) {
    if (segSigma == sigma) {
      // Blur level is same as the one used for bit sampling, so just
      // copy that result
      fimSeg.copy_from(fim);
    } else {
      // blur anew
      fimSeg.copy_from(fimOrig);

      int filterSize = ((int) max(3.0f, 3*segSigma)) | 1;
      blurImage(fimSeg, filterSize, segSigma);
    }
  } else {
    // No blur required, so copy original image
    fimSeg.copy_from(fimOrig);
  }

  FloatImage fimTheta(fimSeg.size());
  FloatImage fimMag(fimSeg.size());
  
  // Compute gradient orientation and magnitude for all non-boundary pixels
  // TODO: This could be made faster by mapping the data with Eigen matrices and
  //       computing gradients in single commands
  for (int y = 1; y < height-1; ++y) {
    for (int x = 1; x < width-1; ++x) {
      float Ix = fimSeg[y][x+1] - fimSeg[y][x-1];
      float Iy = fimSeg[y+1][x] - fimSeg[y-1][x];

      float mag = Ix*Ix + Iy*Iy;
#if 0 // kaess: fast version, but maybe less accurate?
      float theta = MathUtil::fast_atan2(Iy, Ix);
#else
      float theta = atan2(Iy, Ix);
#endif

      fimTheta[y][x] = theta;
      fimMag[y][x] = mag;
    }
  }

  //================================================================
  // Step three. Extract edges by grouping pixels with similar
  // thetas together. This is a greedy algorithm: we start with
  // the most similar pixels.  We use 4-connectivity.

  int numPixels = fimSeg.size().area();
  UnionFindSimple uf(numPixels);
  
  vector<Edge> edges(numPixels*4);
  size_t nEdges = 0;

  // Bounds on the thetas assigned to this group. Note that because
  // theta is periodic, these are defined such that the average
  // value is contained *within* the interval.
  { // limit scope of storage
    /* Previously all this was on the stack, but this is 1.2MB for 320x240 images
     * That's already a problem for OS X (default 512KB thread stack size),
     * could be a problem elsewhere for bigger images... so store on heap */
    vector<float> storage(numPixels*4);  // do all the memory in one big block, exception safe
    float * tmin = &storage[numPixels*0];
    float * tmax = &storage[numPixels*1];
    float * mmin = &storage[numPixels*2];
    float * mmax = &storage[numPixels*3];

    for (int y = 0; y+1 < height; ++y) {
      for (int x = 0; x+1 < width; ++x) {

        float mag0 = fimMag[y][x];
        if (mag0 < Edge::minMag)
          continue;
        mmax[y*width+x] = mag0;
        mmin[y*width+x] = mag0;

        float theta0 = fimTheta[y][x];
        tmin[y*width+x] = theta0;
        tmax[y*width+x] = theta0;

        // Calculates then adds edges to 'vector<Edge> edges'
        Edge::calcEdges(theta0, x, y, fimTheta, fimMag, edges, nEdges);

        // XXX Would 8 connectivity help for rotated tags?
        // Probably not much, so long as input filtering hasn't been disabled.
      }
    }

    edges.resize(nEdges);
    std::stable_sort(edges.begin(), edges.end());
    Edge::mergeEdges(edges,uf,tmin,tmax,mmin,mmax);
  }

  //================================================================
  // Step four: Loop over the pixels again, collecting statistics for each cluster.
  // We will soon fit lines (segments) to these points.

  map<int, vector<XYWeight> > clusters;
  for (int y = 0; y+1 < height; ++y) {
    for (int x = 0; x+1 < width; ++x) {
      if (uf.getSetSize(y*width+x) < Segment::minimumSegmentSize)
        continue;

      int rep = (int) uf.getRepresentative(y*width+x);

      map<int, vector<XYWeight> >::iterator it = clusters.find(rep);
      if ( it == clusters.end() ) {
        clusters[rep] = vector<XYWeight>();
        it = clusters.find(rep);
      }
      vector<XYWeight> &points = it->second;
      points.push_back(XYWeight(x,y,fimMag[y][x]));
    }
  }

  //================================================================
  // Step five: Loop over the clusters, fitting lines (which we call Segments).
  std::vector<Segment> segments; //used in Step six
  std::map<int, std::vector<XYWeight> >::const_iterator clustersItr;
  for (clustersItr = clusters.begin(); clustersItr != clusters.end(); clustersItr++) {
    std::vector<XYWeight> points = clustersItr->second;
    GLineSegment2D gseg = GLineSegment2D::lsqFitXYW(points);

    // filter short lines
    float length = MathUtil::distance2D(gseg.getP0(), gseg.getP1());
    if (length < Segment::minimumLineLength)
      continue;

    Segment seg;
    float dy = gseg.getP1().second - gseg.getP0().second;
    float dx = gseg.getP1().first - gseg.getP0().first;

    float tmpTheta = std::atan2(dy,dx);

    seg.setTheta(tmpTheta);
    seg.setLength(length);

    // We add an extra semantic to segments: the vector
    // p1->p2 will have dark on the left, white on the right.
    // To do this, we'll look at every gradient and each one
    // will vote for which way they think the gradient should
    // go. This is way more retentive than necessary: we
    // could probably sample just one point!

    float flip = 0, noflip = 0;
    for (unsigned int i = 0; i < points.size(); i++) {
      XYWeight xyw = points[i];

      int xWhole = (int) xyw.x;
      int yWhole = (int) xyw.y;
      float theta = fimTheta[yWhole][xWhole];
      float mag = fimMag[yWhole][xWhole];

      // err *should* be +M_PI/2 for the correct winding, but if we
      // got the wrong winding, it'll be around -M_PI/2.
      float err = MathUtil::mod2pi(theta - seg.getTheta());

      if (err < 0)
        noflip += mag;
      else
        flip += mag;
    }

    if (flip > noflip) {
      float temp = seg.getTheta() + (float)M_PI;
      seg.setTheta(temp);
    }

    float dot = dx*std::cos(seg.getTheta()) + dy*std::sin(seg.getTheta());
    if (dot > 0) {
      seg.setX0(gseg.getP1().first); seg.setY0(gseg.getP1().second);
      seg.setX1(gseg.getP0().first); seg.setY1(gseg.getP0().second);
    }
    else {
      seg.setX0(gseg.getP0().first); seg.setY0(gseg.getP0().second);
      seg.setX1(gseg.getP1().first); seg.setY1(gseg.getP1().second);
    }

    segments.push_back(seg);
  }

  // Step six: For each segment, find segments that begin where this segment ends.
  // (We will chain segments together next...) The gridder accelerates the search by
  // building (essentially) a 2D hash table.
  Gridder<Segment> gridder(0,0,width,height,10);
  
  // add every segment to the hash table according to the position of the segment's
  // first point. Remember that the first point has a specific meaning due to our
  // left-hand rule above.
  for (unsigned int i = 0; i < segments.size(); i++) {
    gridder.add(segments[i].getX0(), segments[i].getY0(), &segments[i]);
  }
  
  // Now, find child segments that begin where each parent segment ends.
  for (unsigned i = 0; i < segments.size(); i++) {
    Segment &parentseg = segments[i];

    //compute length of the line segment
    GLine2D parentLine(std::pair<float,float>(parentseg.getX0(), parentseg.getY0()),
                       std::pair<float,float>(parentseg.getX1(), parentseg.getY1()));

    Gridder<Segment>::iterator iter = gridder.find(parentseg.getX1(), parentseg.getY1(), 0.5f*parentseg.getLength());
    while(iter.hasNext()) {
      Segment &child = iter.next();
      if (MathUtil::mod2pi(child.getTheta() - parentseg.getTheta()) > 0) {
        continue;
      }

      // compute intersection of points
      GLine2D childLine(std::pair<float,float>(child.getX0(), child.getY0()),
                        std::pair<float,float>(child.getX1(), child.getY1()));

      std::pair<float,float> p = parentLine.intersectionWith(childLine);
      if (p.first == -1) {
        continue;
      }

      float parentDist = MathUtil::distance2D(p, std::pair<float,float>(parentseg.getX1(),parentseg.getY1()));
      float childDist = MathUtil::distance2D(p, std::pair<float,float>(child.getX0(),child.getY0()));

      if (max(parentDist,childDist) > parentseg.getLength()) {
        // cout << "intersection too far" << endl;
        continue;
      }

      // everything's OK, this child is a reasonable successor.
      parentseg.children.push_back(&child);
    }
  }

  //================================================================
  // Step seven: Search all connected segments to see if any form a loop of length 4.
  // Add those to the quads list.
  vector<Quad> quads;
  
  vector<Segment*> tmp(5);
  for (unsigned int i = 0; i < segments.size(); i++) {
    tmp[0] = &segments[i];
    Quad::search(tmp, segments[i], 0, quads);
  }

  //================================================================
  // Step eight. Decode the quads. For each quad, we first estimate a
  // threshold color to decide between 0 and 1. Then, we read off the
  // bits and see if they make sense.

  std::vector<TagDetection> detections;

  for (unsigned int qi = 0; qi < quads.size(); qi++ ) {
    Quad &quad = quads[qi];

    // Find a threshold
    GrayModel blackModel, whiteModel;

    // Expected number of 'bits' in x and y directions in tag from given tag family.
    // This includes the solid black border around the tag.
    const int dd = 2 * thisTagFamily.blackBorder + thisTagFamily.dimension;

    // Now we build a model of how we expect white and black pixel values to vary over the image.
    // Loop over all locations where we expect a 'bit' to reside in the target.
    // Also include white border around the black border.
    for (int iy = -1; iy <= dd; iy++) {
      float y = (iy + 0.5f) / dd;
      for (int ix = -1; ix <= dd; ix++) {
        float x = (ix + 0.5f) / dd;

        // Get pixel location of current 'bit'
        std::pair<float,float> pxy = quad.interpolate01(x, y);
        int irx = (int) (pxy.first + 0.5);
        int iry = (int) (pxy.second + 0.5);
        if (irx < 0 || irx >= width || iry < 0 || iry >= height) {
          // TODO: We should completely bail on this quad now, as part of it is off-screen
          //       Currently we only bail on the inner loop...
          continue;
        }

        // Look up the pixel value
        float v = fim[iry][irx];

        if (iy == -1 || iy == dd || ix == -1 || ix == dd) {
          // Pixel is in the white border area, so add it to the White model
          whiteModel.addObservation(x, y, v);
        } else if (iy == 0 || iy == (dd-1) || ix == 0 || ix == (dd-1)) {
          // Pixel is in the black border area, so add it to the Black model
          blackModel.addObservation(x, y, v);
        }
      }
    }

    // Try to read the tag's code
    // The threshold between black and white is spatially varying and obtained using the
    // models computed in the previous step.

    // Will be set to true if part of the tag falls outside of the image
    // TODO: Surely the previous loop computing gray models will have rejected the quad already
    //       if it's outside the image?
    bool bad = false;

    // After the loop finishes, this will contain the tag code
    unsigned long long tagCode = 0;

    for ( int iy = thisTagFamily.dimension-1; iy >= 0; iy-- ) {
      float y = (thisTagFamily.blackBorder + iy + 0.5f) / dd;
      for (int ix = 0; ix < thisTagFamily.dimension; ix++ ) {
        float x = (thisTagFamily.blackBorder + ix + 0.5f) / dd;
        std::pair<float,float> pxy = quad.interpolate01(x, y);
        int irx = (int) (pxy.first + 0.5);
        int iry = (int) (pxy.second + 0.5);
        if (irx < 0 || irx >= width || iry < 0 || iry >= height) {
          // cout << "*** bad:  irx=" << irx << "  iry=" << iry << endl;
          bad = true;
          continue;
        }
        float threshold = (blackModel.interpolate(x,y) + whiteModel.interpolate(x,y)) * 0.5f;
        float v = fim[iry][irx];
        tagCode = tagCode << 1;
        if ( v > threshold)
          tagCode |= 1;
      }
    }

    if ( !bad ) {
      TagDetection thisTagDetection;
      thisTagFamily.decode(thisTagDetection, tagCode);

      // Rotate points in detection according to decoded
      // orientation.  Thus the order of the points in the
      // detection object can be used to determine the
      // orientation of the target.
      for (int i=0; i< 4; i++) {
        thisTagDetection.p[i] = quad.quadPoints[(i+thisTagDetection.rotation) % 4];
      }

      if (thisTagDetection.good) {
        thisTagDetection.cxy = quad.interpolate01(0.5f, 0.5f);
        thisTagDetection.observedPerimeter = quad.observedPerimeter;
        detections.push_back(thisTagDetection);
      }
    }
  }

  //================================================================
  //Step nine: Some quads may be detected more than once, due to
  //partial occlusion and our aggressive attempts to recover from
  //broken lines. When two quads (with the same id) overlap, we will
  //keep the one with the lowest error, and if the error is the same,
  //the one with the greatest observed perimeter.

  std::vector<TagDetection> goodDetections;

  // NOTE: allow multiple non-overlapping detections of the same target.

  for ( vector<TagDetection>::const_iterator it = detections.begin();
        it != detections.end(); it++ ) {
    const TagDetection &thisTagDetection = *it;

    bool newFeature = true;

    for ( unsigned int odidx = 0; odidx < goodDetections.size(); odidx++) {
      TagDetection &otherTagDetection = goodDetections[odidx];

      if ( thisTagDetection.id != otherTagDetection.id ||
           ! thisTagDetection.overlapsTooMuch(otherTagDetection) )
        continue;

      // There's a conflict.  We must pick one to keep.
      newFeature = false;

      // This detection is worse than the previous one... just don't use it.
      if ( thisTagDetection.hammingDistance > otherTagDetection.hammingDistance )
        continue;

      // Otherwise, keep the new one if it either has strictly *lower* error, or greater perimeter.
      // TODO: This doesn't quite do what the comment above says it does.  This will preferentially take a
      //       detection with much more error if the perimeter is larger
      if ( thisTagDetection.hammingDistance < otherTagDetection.hammingDistance ||
           thisTagDetection.observedPerimeter > otherTagDetection.observedPerimeter )
        goodDetections[odidx] = thisTagDetection;
    }

    if ( newFeature ) {
      goodDetections.push_back(thisTagDetection);
    }

  }

  //cout << "AprilTags: edges=" << nEdges << " clusters=" << clusters.size() << " segments=" << segments.size()
  //     << " quads=" << quads.size() << " detections=" << detections.size() << " unique tags=" << goodDetections.size() << endl;

  return goodDetections;
}

} // namespace
