#include "AprilTags/FloatImage.h"

namespace AprilTags {

void convertByteImageToFloatImage(const ImageType& grayImage,
                                  FloatImage &outImage) {
  outImage.resize(grayImage.size());

  int width = grayImage.size().x;
  int height = grayImage.size().y;

  for (int i = 0; i<width*height; ++i) {
      outImage.data()[i] = grayImage.data()[i]/255.0;
  }
}

} // namespace AprilTags
